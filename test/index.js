import assert from 'assert'
import fs from 'fs'

import MarkdownIt from 'markdown-it'
import MarkdownItIndent from '../index.js'

describe('markdown-it indent', () => {

	const md = new MarkdownIt().use(MarkdownItIndent)

	it('First test', () => {
		const input = fs.readFileSync('test/fixtures/input1.html', 'utf8')
		const gt = fs.readFileSync('test/fixtures/gt1.html', 'utf8')
		const output = md.render(input)
		assert.equal(
			output.replaceAll('\n', '').replaceAll('\t', ''), 
			gt.replaceAll('\n', '').replaceAll('\t', '')
		)
	})

})