let curIndent = -4
let prevSection = -1

export default function section(state, startLine, endLine) {

	let pos = state.bMarks[startLine] + state.tShift[startLine]

	if (!state.isEmpty(startLine) && state.sCount[startLine] - curIndent >= 4) {
		// console.log(`Child section start found at ${startLine}`)
		curIndent += 4
	} else if (!state.isEmpty(startLine) && curIndent - state.sCount[startLine] >= 4) {
		// console.log(`Parent section start found at ${startLine}`)
		curIndent -= 4
	} else if (startLine != prevSection && curIndent == state.sCount[startLine] && state.src.charCodeAt(pos) == 0x23) {
		// console.log(`Same level section start found at ${startLine}`)
	} else {
		return false
	}

	const startSection = startLine
	prevSection = startSection

	let endSection = startSection + 1
	while(endSection < endLine) {
		if (!state.isEmpty(endSection) && state.sCount[endSection] - curIndent <= -4) break

		let pos = state.bMarks[endSection + 1] + state.tShift[endSection + 1]
		if (state.sCount[endSection + 1] == curIndent && state.src.charCodeAt(pos) == 0x23) break
		endSection++
	}

	const blkIndent = state.blkIndent
	state.blkIndent = curIndent

	const parentType = state.parentType
	state.parentType = 'section'

	// Open
	let token = state.push('section_open', 'section', 1)
	token.map = [startSection, endSection]
	token.block  = true

	state.md.block.tokenize(state, startSection, endSection)

	// Close
	token = state.push('section_close', 'section', -1)
	token.block  = true
	token.markup = state.src.slice(startSection, endSection)
	state.line = endSection

	state.blkIndent = blkIndent
	state.parentType = parentType

	return true
}