import heading from './src/rules/heading.js'
import section from './src/rules/section.js'
import include from './src/rules/include.js'

export default function register(md, name, options) {
	md.core.ruler.before('normalize', 'include', include)
	// md.core.ruler.before('inline', 'section', section)
	md.block.ruler.at('code', section)
	md.block.ruler.at('heading', heading)
	return md
}