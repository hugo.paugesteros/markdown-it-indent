import fs from 'fs'
import path from 'path'

// const INCLUDE_RE = /([	]*)!{3}\s*include\((.+?)\)!{3}/ig
const INCLUDE_RE = /([	]*)@\s*include\((.+?)\)/i
const ROOT = '.'

export default function include(state, startLine, endLine) {

	function _includeFile(source, relativeRoot) {
		
		// While there are matches in the source
		let result
		while (result = INCLUDE_RE.exec(source)) {
			
			const indentLevel = result[1].length
			const includePath = result[2].trim()

			const filePath = path.resolve(relativeRoot, includePath)
			
			let child = ''
			if (fs.existsSync(filePath)) {
				child = _includeFile(fs.readFileSync(filePath, 'utf8'), path.dirname(filePath))
				child = result[1] + child.split('\n').join(`\n${result[1]}`)
			}
			source = source.slice(0, result.index) + child + source.slice(result.index + result[0].length, source.length)

		}

		return source
			
	}

	state.src = _includeFile(state.src, ROOT)

}